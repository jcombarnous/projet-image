import torch.nn as nn

class GenerativeModel(nn.Module):
    def __init__(self,  num_classes=3, latent_dim=128):
        super(GenerativeModel, self).__init__()
       
        # Encoder
        self.encoder = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(256, latent_dim, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(latent_dim),
            nn.ReLU()
        )
        
        self.decoder = nn.Sequential(
            nn.Conv2d(latent_dim, 256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Upsample(scale_factor=2,  mode='nearest'),
            nn.Conv2d(256, 128, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(128, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Upsample(scale_factor=2,  mode='nearest'),
            nn.Conv2d(64, 32, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 3, kernel_size=3, stride=1, padding=1),
            nn.Upsample(size=(256, 256),  mode='nearest'),
            nn.Tanh()
        )

        # Segmentation head
        self.segmentation_head = nn.Sequential( 
            nn.Conv2d(latent_dim, num_classes, kernel_size=1),
            nn.Upsample(size=(256, 256),  mode='nearest')
            )


    def forward(self, input_image):
        # Encode
        latent_representation = self.encoder(input_image)

        # Decode
        generated_image = self.decoder(latent_representation)

        # Segmentation
        segmentation_map = self.segmentation_head(latent_representation)

        return generated_image, segmentation_map


