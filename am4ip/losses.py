#losses.py
import torch
# noinspection PyProtectedMember
from torch.nn.modules.loss import _Loss

import torch.nn as nn
import torch.nn.functional as F


class TVLoss(_Loss):
    def __init__(self):
        super(TVLoss, self).__init__()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError


class AsymmetricLoss(_Loss):
    def __init__(self):
        super(AsymmetricLoss, self).__init__()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError


class TotalLoss(_Loss):
    def __init__(self, lambda_tv, lambda_asymm):
        super(TotalLoss, self).__init__()
        self.lambda_tv = lambda_tv
        self.lambda_asymm = lambda_asymm
        self.loss = torch.nn.CrossEntropyLoss()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        loss = self.loss(inp,target)
        return loss

class FocalLoss(nn.Module):
    def __init__(self, alpha=None, gamma=2):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma

    def forward(self, inputs, targets):
        ce_loss = F.cross_entropy(inputs, targets, reduction='none')
        pt = torch.exp(-ce_loss)
        # to check that alpha is on the same device as targets
        alpha = self.alpha.to(targets.device)

        loss = (alpha[targets] * (1 - pt) ** self.gamma * ce_loss).mean()
        return loss
