from typing import Callable, List
import torch
import torch.utils.data as data
import time

class BaselineTrainer:
    def __init__(self, model: torch.nn.Module,
                 loss: Callable,
                 optimizer: torch.optim.Optimizer,
                 use_cuda=True):
        self.loss = loss
        self.use_cuda = use_cuda
        self.optimizer = optimizer

        if use_cuda:
            self.model = model.to(device="cuda:0")

    def fit(self, train_data_loader: data.DataLoader,
            val_data_loader: data.DataLoader,
            epoch: int):
        train_loss_history = []  # Added to store training loss history
        train_accuracy_history = []  # Added to store training accuracy history
        val_loss_history = []  # Added to store validation loss history
        val_accuracy_history = []  # Added to store validation accuracy history
        self.model.train()  # Use model.train() instead of setting self.model.training

        for e in range(epoch):
            print(f"Start epoch {e+1}/{epoch}")
            n_batch_train = 0
            avg_loss_train = 0.0
            correct_train = 0
            total_train = 0

            # Training
            for i, (ref_img, y) in enumerate(train_data_loader):
                # Reset previous gradients
                self.optimizer.zero_grad()

                # Move data to cuda if necessary:
                if self.use_cuda:
                    ref_img = ref_img.cuda()
                    y = y.cuda()

                # Make forward
                outputs = self.model.forward(ref_img)
                loss = self.loss(outputs, y)
                loss.backward()

                # Adjust learning weights
                self.optimizer.step()
                avg_loss_train += loss.item()
                n_batch_train += 1

                _, predicted = torch.max(outputs.data, 1)
                total_train += y.size(0)*y.size(1)*y.size(2)
                correct_train += (predicted == y).sum().item()

                print(f"\rTraining {i+1}/{len(train_data_loader)}: loss = {avg_loss_train / n_batch_train}", end='')

            train_loss_history.append(avg_loss_train / n_batch_train)  # Record training loss for this epoch
            train_accuracy_history.append(correct_train / total_train)  # Record training accuracy for this epoch
            
            # Validation
            self.model.eval()  # Set model to evaluation mode
            avg_loss_val = 0.0
            correct_val = 0
            total_val = 0
            n_batch_val = 0

            with torch.no_grad():
                for i, (ref_img, y) in enumerate(val_data_loader):
                    if self.use_cuda:
                        ref_img = ref_img.cuda()
                        y = y.cuda()

                    outputs_val = self.model.forward(ref_img)
                    loss_val = self.loss(outputs_val, y)
                    avg_loss_val += loss_val.item()
                    n_batch_val += 1

                    _, predicted_val = torch.max(outputs_val.data, 1)
                    total_val += y.size(0)*y.size(1)*y.size(2)
                    correct_val += (predicted_val == y).sum().item()

                    print(f"\rValidation {i+1}/{len(val_data_loader)}: loss = {avg_loss_val / n_batch_val}", end='')

            val_loss_history.append(avg_loss_val / n_batch_val)  # Record validation loss for this epoch
            val_accuracy_history.append(correct_val / total_val)  # Record validation accuracy for this epoch
            self.model.train()  # Set model back to training mode

            print()
            # Sauvegarde du modèle toutes les 5 époques
            if((e + 1) % 5 == 0):
                torch.save(self.model, '/net/cremi/jcombarnous/espaces/travail/TraitementImage/M2/projet-image/unet_results/unet_epoch_' + str(e + 1) + '.pt')

        return {
            'train_loss': train_loss_history,
            'train_accuracy': train_accuracy_history,
            'val_loss': val_loss_history,
            'val_accuracy': val_accuracy_history,
        }